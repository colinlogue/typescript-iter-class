This module has two parts:

1. `iter-func.ts`, which provides iterator combinators (that
is, functions that take an iterator as input and return an
iterator as output) and a few other useful functions.
2. `iter.ts`, which provides a class `Iter` that implements
each of the functions as a method, allowing chaining.

Someday we will be granted a [pipeline operator](https://github.com/tc39/proposal-pipeline-operator)
and chaining functions in a reasonable way will become
possible. Until then, we're stuck needing an object that
implements the functions as methods. That's all `Iter` does.

## Examples

Split a set into disjoint subsets:

```typescript
const numbers = new Set([1,2,3,4,5,6,7]);
const [ evens, odds ] = iter(numbers)
  .partition(x => x % 2 == 0)
  .map(toSet);

console.log(evens); // Set(3) { 2, 4, 6 }

console.log(odds); // Set(4) { 1, 3, 5, 7 }
```
