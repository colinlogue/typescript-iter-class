import { parse } from "https://deno.land/std@0.175.0/flags/mod.ts";
import { removeTSExtensions } from "./remove-ts-extensions.ts";
import { runNPM } from "./run-npm.ts";

const srcFiles = [
  'iter.ts',
  'iter-func.ts',
  'mod.ts'
];
const srcDirPath = 'src';
const destDirPath = 'node/dist/ts';


export async function compileNPM() {
  await removeTSExtensions(srcFiles, srcDirPath, destDirPath);
  return runNPM(['run', 'compile'], { cwd: 'node' });
}

export async function packageNPM() {
  return runNPM(['pack'], { cwd: 'node' });
}

if (import.meta.main) {
  const flags = parse(Deno.args, {
    boolean: ['compile', 'pack'],
    default: {
      compile: true,
      pack: false,
    },
  });
  if (flags.compile) {
    await compileNPM();
  }
  if (flags.pack) {
    await packageNPM();
  }
}