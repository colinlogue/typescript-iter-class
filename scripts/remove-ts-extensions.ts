import * as path from "https://deno.land/std@0.175.0/path/mod.ts";

export async function removeTSExtensions(
  srcFiles: string[],
  srcDirPath: string,
  destDirPath: string
): Promise<void> {

  await Deno.mkdir(destDirPath, { recursive: true });

  await Promise.all(
    srcFiles.map(async srcFile => {
      const srcPath = path.join(srcDirPath, srcFile);
      let text = await Deno.readTextFile(srcPath);
      text = text.replaceAll(
        /((?:import|export)\s*(?:\*|\{(?:\s*\w+,?)*\s\})\s*from\s*['"][\.\/\w\-]+)\.ts(['"])/g,
        "$1$2"
      );
      const destPath = path.join(destDirPath, srcFile);
      Deno.writeTextFile(destPath, text);
    })
  );

}