import { which } from "https://deno.land/x/which@0.2.2/mod.ts";

export type RunNPMOptions = Omit<Deno.RunOptions, 'cmd'>
export async function runNPM(args: string[], options: RunNPMOptions = {}): Promise<Deno.ProcessStatus> {
  const path = await which('npm');
  if (!path) {
    throw new Error('npm command not found');
  }
  const process = Deno.run({
    ...options,
    cmd: [path, ...args],
  })
  return process.status();
}