
export type nullish = null | undefined;

export function isNullish(val: unknown): val is nullish {
  return val === null || val === undefined;
}

export function* map<T1, T2>(iter: IterableIterator<T1>, mapper: (val: T1) => T2): Generator<T2, void> {
  for (const val of iter) {
    yield mapper(val);
  }
}

export function* chain<T>(...iters: IterableIterator<T>[]): Generator<T, void> {
  for (const iter of iters) {
    for (const val of iter) {
      yield val;
    }
  }
}

export function count(iter: IterableIterator<unknown>): number {
  let n = 0;
  for (const _ of iter) {
    ++n;
  }
  return n;
}

export function last<T>(iter: IterableIterator<T>): T | null {
  let final: T | null = null;
  for (const val of iter) {
    final = val;
  }
  return final;
}

export function* skip<T>(iter: IterableIterator<T>, n: number): Generator<T, void> {
  for (const val of iter) {
    if (n > 0) {
      --n;
    }
    else {
      yield val;
    }
  }
}

export function nth<T>(iter: IterableIterator<T>, n: number): T | null {
  if (n < 0) {
    return null;
  }
  for (const val of iter) {
    if (n === 0) {
      return val;
    }
    --n;
  }
  return null;
}

export function* range(start: number, end: number, step = 1): Generator<number, void> {
  while (start < end) {
    yield start;
    start += step;
  }
}

export function take<T>(iter: IterableIterator<T>, n: number): T[] {
  let i = 0;
  const items: T[] = [];
  for (const item of iter) {
    if (i < n) {
      items.push(item);
      ++i;
    }
    else {
      break;
    }
  }
  return items;
}

export function* zip<T1, T2>(iter1: IterableIterator<T1>, iter2: Iterable<T2>): Generator<[T1, T2], void> {
  const iterator2 = iter2[Symbol.iterator]();
  for (const item1 of iter1) {
    const result2 = iterator2.next();
    if (result2.done) {
      return;
    }
    const item2 = result2.value;
    yield [item1, item2];
  }
}

export function* stepBy<T>(iter: IterableIterator<T>, step: number): Generator<T, void> {
  if (step < 1) {
    throw new Error('expected step of at least 1, got ' + step);
  }
  let i = 1;
  for (const val of iter) {
    if (i === step) {
      yield val;
      i = 1;
    }
    ++i;
  }
}

export function* intersperse<T1, T2>(iter: IterableIterator<T1>, separator: T2): Generator<T1 | T2, void> {
  const result = iter.next();
  if (result.done) {
    return;
  }
  yield result.value;
  for (const val of iter) {
    yield separator;
    yield val;
  }
}

export function* intersperseWith<T1, T2>(iter: IterableIterator<T1>, getSeparator: (val: T1) => T2): Generator<T1 | T2, void> {
  const result = iter.next();
  if (result.done) {
    return;
  }
  yield result.value;
  let previous = result.value;
  for (const val of iter) {
    yield getSeparator(previous);
    yield val;
    previous = val;
  }
}

export const BREAK = Symbol();

export function forEach<T>(iter: IterableIterator<T>, action: (val: T) => typeof BREAK | void): void {
  for (const val of iter) {
    if (action(val) === BREAK) {
      break;
    }
  }
}

export function* filter<T>(iter: IterableIterator<T>, predicate: (val: T) => boolean): Generator<T, void> {
  for (const val of iter) {
    if (predicate(val)) {
      yield val;
    }
  }
}

/**
 * Acts like `filter`, but constrains the output type based on the predicate.
 * @param iter
 * @param predicate
 */
export function* refine<T, U extends T>(iter: IterableIterator<T>, predicate: (val: T) => val is U): Generator<U, void> {
  for (const val of iter) {
    if (predicate(val)) {
      yield val;
    }
  }
}

export function* filterMap<T1, T2>(iter: IterableIterator<T1>, f: (val: T1) => T2 | nullish): Generator<T2, void> {
  for (const val of iter) {
    const val2 = f(val);
    if (!(val2 === null || val2 === undefined)) {
      yield val2;
    }
  }
}

export function* enumerate<T>(iter: IterableIterator<T>): Generator<[number, T], void> {
  let i = 0;
  for (const val of iter) {
    yield [i, val];
    ++i;
  }
}

export interface Peekable<T> {
  peek(): IteratorResult<T, void>;
}

export type PeekableIterableIterator<T, I extends IterableIterator<T>> = I & Peekable<T> & {
  [Symbol.iterator](): PeekableIterableIterator<T, I>;
}

export function peekable<T, I extends IterableIterator<T>>(iter: I): PeekableIterableIterator<T, I> {
  let peeked: IteratorResult<T, void> | null = null;
  const i: PeekableIterableIterator<T, I> = {
    ...iter,
    [Symbol.iterator](this: PeekableIterableIterator<T, I>): PeekableIterableIterator<T, I> {
      return this;
    },
    next(): IteratorResult<T, void> {
      if (peeked) {
        const result = peeked;
        peeked = null;
        return result;
      }
      return this.next();
    },
    peek(): IteratorResult<T, void> {
      peeked ??= this.next();
      return peeked;
    },
  };
  const { return: _return, throw: _throw } = iter;
  if (_return) {
    i.return = function(...args) {
      peeked = _return(...args);
      return peeked;
    };
  }
  if (_throw) {
    i.throw = function(...args) {
      peeked = _throw(...args);
      return peeked;
    }
  }
  return i;
}

export function* skipWhile<T>(iter: IterableIterator<T>, predicate: (val: T) => boolean): Generator<T, void> {
  for (const val of iter) {
    if (!predicate(val)) {
      yield val;
    }
  }
}

export function takeWhile<T>(iter: IterableIterator<T>, predicate: (val: T) => boolean): T[] {
  const vals: T[] = [];
  for (const val of iter) {
    if (predicate(val)) {
      vals.push(val);
    }
    else {
      break;
    }
  }
  return vals;
}

/**
 * Splits an iterable into a tuple of two arrays based on a predicate, where all
 * of the elements of the left array satisfy the predicate and all of the
 * elements of the right array do not.
 *
 * For a similar function that provides type narrowing, see `partitionRefine`.
 */
export function partition<T>(iter: Iterable<T>, predicate: (val: T) => boolean): [ T[], T[] ] {
  const left: T[] = [];
  const right: T[] = [];
  for (const val of iter) {
    if (predicate(val)) {
      left.push(val);
    }
    else {
      right.push(val);
    }
  }
  return [left, right];
}

/**
 * Splits an iterable in the same manner as `partition`, but narrows the types
 * of the result arrays as well.
 *
 * Note that because of the way TypeScript's type predicates work, it may be the
 * case that elements in the right array do in fact have type `U`, so we can't
 * safely narrow the type of the right array. If you are certain that your
 * predicate does not return any false negatives (that is, for all `x: U`,
 * `predicate(x) == true`) then you can cast the result to type
 * `[ U[], Exclude<T, U>[] ]` to gain that narrowing for the right array.
 */
export function partitionRefine<T, U extends T>(iter: Iterable<T>, predicate: (val: T) => val is U): [ U[], T[] ] {
  const left: U[] = [];
  const right: T[] = [];
  for (const val of iter) {
    if (predicate(val)) {
      left.push(val);
    }
    else {
      right.push(val);
    }
  }
  return [left, right];
}

export function fold<T1, T2>(iter: IterableIterator<T1>, initial: T2, reducer: (acc: T2, val: T1) => T2): T2 {
  let acc = initial;
  for (const val of iter) {
    acc = reducer(acc, val);
  }
  return acc;
}

export function reduce<T>(iter: IterableIterator<T>, reducer: (acc: T, val: T) => T): T | null {
  const first = take(iter, 1);
  if (first.length === 1) {
    const [ initial ] = first;
    return fold(iter, initial, reducer);
  }
  return null;
}

export function all<T>(iter: IterableIterator<T>, predicate: (val: T) => boolean): boolean {
  for (const val of iter) {
    if (!predicate(val)) {
      return false;
    }
  }
  return true;
}

export function any<T>(iter: IterableIterator<T>, predicate: (val: T) => boolean): boolean {
  for (const val of iter) {
    if (predicate(val)) {
      return true;
    }
  }
  return false;
}

export function* scan<T1, T2>(iter: IterableIterator<T1>, initial: T2, reducer: (acc: T2, val: T1) => T2 | nullish): Generator<T2, void> {
  let acc: T2 | nullish = initial;
  for (const val of iter) {
    acc = reducer(acc, val);
    if (isNullish(acc)) {
      return;
    }
    yield acc;
  }
}

export function find<T>(iter: IterableIterator<T>, predicate: (val: T) => boolean): T | null {
  for (const val of iter) {
    if (predicate(val)) {
      return val;
    }
  }
  return null;
}

export function findRefine<T, U extends T>(iter: IterableIterator<T>, predicate: (val: T) => val is U): U | null {
  for (const val of iter) {
    if (predicate(val)) {
      return val;
    }
  }
  return null;
}

export function findMap<T1, T2>(iter: IterableIterator<T1>, mapper: (val: T1) => T2 | nullish): T2 | null {
  for (const val of iter) {
    const result = mapper(val);
    if (!isNullish(result)) {
      return result;
    }
  }
  return null;
}

export function position<T>(iter: IterableIterator<T>, predicate: (val: T) => boolean): number | null {
  let i = 0;
  for (const val of iter) {
    if (predicate(val)) {
      return i;
    }
    ++i;
  }
  return null;
}
