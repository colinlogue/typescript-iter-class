import { BREAK, chain, count, enumerate, filter, filterMap, forEach, intersperse, intersperseWith, last, map, nth, nullish, partition, peekable, Peekable, PeekableIterableIterator, refine, partitionRefine, skip, skipWhile, stepBy, take, takeWhile, zip, fold, reduce, all, any, scan, find, findRefine, findMap, position } from "./iter-func.ts";


/** Shortcut for creating an `Iter` from an `Iterable`. */
export function iter<T>(source: Iterable<T>): Iter<T> {
  return Iter.from(source);
}

export class Iter<T> implements IterableIterator<T> {

  static from<T>(source: Iterable<T>): Iter<T> {
    const iterator = source[Symbol.iterator]();
    return new Iter(iterator);
  }

  [Symbol.iterator]() {
    return this;
  }

  readonly next: () => IteratorResult<T, unknown>;

  private constructor(iterator: Iterator<T, unknown, unknown>) {
    this.next = () => iterator.next();
  }

  map<T2>(mapper: (val: T) => T2): Iter<T2> {
    return new Iter(map(this, mapper));
  }

  chain(...others: IterableIterator<T>[]): Iter<T> {
    return new Iter(chain(this, ...others));
  }

  count(): number {
    return count(this);
  }

  last(): T | null {
    return last(this);
  }

  skip(n: number): Iter<T> {
    return new Iter(skip(this, n));
  }

  nth(n: number): T | null {
    return nth(this, n);
  }

  take(n: number): T[] {
    return take(this, n);
  }

  zip<T2>(other: IterableIterator<T2>): Iter<[T,T2]> {
    return new Iter(zip(this, other));
  }

  stepBy(step: number): Iter<T> {
    return new Iter(stepBy(this, step));
  }

  intersperse<T2>(separator: T2): Iter<T | T2> {
    return new Iter(intersperse(this, separator));
  }

  intersperseWith<T2>(getSeparator: (val: T) => T2): Iter<T | T2> {
    return new Iter(intersperseWith(this, getSeparator));
  }

  forEach(action: (val: T) => typeof BREAK | void): void {
    forEach(this, action);
  }

  filter(predicate: (val: T) => boolean): Iter<T> {
    return new Iter(filter(this, predicate));
  }

  refine<U extends T>(predicate: (val: T) => val is U): Iter<U> {
    return new Iter(refine(this, predicate));
  }

  filterMap<T2>(f: (val: T) => T2 | nullish): Iter<T2> {
    return new Iter(filterMap(this, f));
  }

  enumerate(): Iter<[number, T]> {
    return new Iter(enumerate(this));
  }

  peekable(): PeekableIterableIterator<T, Iter<T>> {
    return peekable(this);
  }

  skipWhile(predicate: (val: T) => boolean): Iter<T> {
    return new Iter(skipWhile(this, predicate));
  }

  takeWhile(predicate: (val: T) => boolean): T[] {
    return takeWhile(this, predicate);
  }

  partition(predicate: (val: T) => boolean): [ T[], T[] ] {
    return partition(this, predicate);
  }

  partitionRefine<U extends T>(predicate: (val: T) => val is U): [ U[], T[] ] {
    return partitionRefine(this, predicate);
  }

  fold<T2>(initial: T2, reducer: (acc: T2, val: T) => T2): T2 {
    return fold(this, initial, reducer);
  }

  reduce(reducer: (acc: T, val: T) => T): T | null {
    return reduce(this, reducer);
  }

  all(predicate: (val: T) => boolean): boolean {
    return all(this, predicate);
  }

  any(predicate: (val: T) => boolean): boolean {
    return any(this, predicate);
  }

  scan<T2>(initial: T2, reducer: (acc: T2, val: T) => T2): Iter<T2> {
    return new Iter(scan(this, initial, reducer));
  }

  find(predicate: (val: T) => boolean): T | null {
    return find(this, predicate);
  }

  findRefine<U extends T>(predicate: (val: T) => val is U): U | null {
    return findRefine(this, predicate);
  }

  findMap<T2>(predicate: (val: T) => T2 | nullish): T2 | null {
    return findMap(this, predicate);
  }

  position(predicate: (val: T) => boolean): number | null {
    return position(this, predicate);
  }

  collect<U>(collector: (iter: Iterable<T>) => U): U {
    return collector(this);
  }

}

export const toSet = <T>(iter: Iterable<T>) => new Set(iter);
export const toArray = <T>(iter: Iterable<T>) => Array.from(iter);
export const toMap = <K, V>(iter: Iterable<[K, V]>) => new Map(iter);
export const toObject = <V>(iter: Iterable<[string, V]>) => {
  const obj: {[k: string]: V} = {};
  for (const [key, val] of iter) {
    obj[key] = val;
  }
  return obj;
}
