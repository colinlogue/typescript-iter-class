import { assert } from "https://deno.land/std@0.175.0/testing/asserts.ts";
import { partition } from "../src/iter-func.ts";
import { isEven } from "./util.ts";

Deno.test('Partition a set of numbers into evens and odds', () => {
  const numbers = new Set([1,2,3,4,5,6,7,8,9]);
  const [ evens, odds ] = partition(numbers.values(), isEven);
  for (const val of evens) {
    assert(isEven(val));
  }
  for (const val of odds) {
    assert(!isEven(val));
  }
});
