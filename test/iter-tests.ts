import { assert, assertEquals, assertInstanceOf } from "https://deno.land/std@0.175.0/testing/asserts.ts";
import { isEven, isPrime, naturals } from "./util.ts";
import { iter, toSet } from "../src/iter.ts";

Deno.test('Partition a set of numbers into evens and odds', () => {
  const numbers = new Set([1,2,3,4,5,6,7,8,9]);
  const [ evens, odds ] = iter(numbers)
    .partition(isEven)
    .map(toSet);
  assertInstanceOf(odds, Set);
  assertInstanceOf(evens, Set);
  for (const val of evens) {
    assert(isEven(val));
  }
  for (const val of odds) {
    assert(!isEven(val));
  }
});

Deno.test('Get iterator of primes', () => {
  const primes = iter(naturals(2))
    .filter(isPrime);

  const actual = primes.take(10);
  const expected = [ 2, 3, 5, 7, 11, 13, 17, 19, 23, 29 ];
  assertEquals(actual, expected);

  for (const val of primes.take(1000)) {
    assert(isPrime(val));
  }
});
