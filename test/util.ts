
export const isEven = (x: number) => x % 2 === 0;

export const isPrime = (x: number) => {
  for (let i = 2; i < x; ++i) {
    if (x % i === 0) {
      return false;
    }
  }
  return true;
};

export function* naturals(start = 0) {
  let i = start;
  while (true) {
    yield i;
    ++i;
  }
}
